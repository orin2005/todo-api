package com.rat.todo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rat.todo.model.Todo;
import com.rat.todo.service.TodoService;

@RequestMapping("/todos")
@RestController
public class TodoController {

	@Autowired
	private TodoService todoService;
	
	@GetMapping
	public ResponseEntity<List<Todo>> helloWorld() {
		List<Todo> todos = todoService.fetchAllTodos();
		return ResponseEntity.ok( todos );
	}
}
