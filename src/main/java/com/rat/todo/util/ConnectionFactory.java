package com.rat.todo.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

	private ConnectionFactory() {}
	
	public static Connection getConnection() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/todo", "postgres", "test");
		} catch(SQLException sqle) {
			sqle.printStackTrace();
		}
		return conn;
	}
	
}
