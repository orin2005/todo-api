package com.rat.todo.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.rat.todo.model.Todo;
import com.rat.todo.util.ConnectionFactory;

@Service
public class TodoService {
	
	public List<Todo> fetchAllTodos() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Todo> todos = new ArrayList<Todo>();
		
		try {
			conn = ConnectionFactory.getConnection();
			pstmt = conn.prepareStatement( "SELECT * FROM todo;" );
			rs = pstmt.executeQuery();
			while( rs.next() ) {
				Todo todo = new Todo();
				todo.setId(rs.getLong("id"));
				todo.setDescription(rs.getString("description"));
				todo.setTitle(rs.getString("title"));
				todo.setSwimlane(rs.getString("swimlane"));
				todos.add(todo);
			}
		} catch(SQLException sqle) {
			sqle.printStackTrace();
		} finally {
			try { rs.close(); } catch (SQLException x) {}
			try { pstmt.close(); } catch (SQLException x) {}
			try { conn.close(); } catch (SQLException x) {}
		}
		return todos;
	}

}
