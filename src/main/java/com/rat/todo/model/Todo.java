package com.rat.todo.model;

public class Todo {

	private long id;
	private String title;
	private String description;
	private String swimlane;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSwimlane() {
		return swimlane;
	}
	public void setSwimlane(String swimlane) {
		this.swimlane = swimlane;
	}
	
	
}
