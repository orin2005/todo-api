CREATE TABLE todo (
	id BIGSERIAL NOT NULL,
	title VARCHAR(255) NOT NULL,
	description TEXT NOT NULL,
	swimlane VARCHAR(50) NOT NULL,
	PRIMARY KEY(id)
);
